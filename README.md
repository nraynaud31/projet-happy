# Projet Happy

# Contexte du projet:
L'association HAPPY souhaite creer un barometre des humeurs du jour mais ne sait pas trop comment s'y prendre, elle fait donc appel a vous pour lui proposer un prototype.
- Un visiteur peut consulter la page d'accueil.
- Chaque utilisateur enregistré pourra selectionner/modifier son humeur du jour.
- Chaque utilisateur enregistré pourra consulter sa tendance d'humeur sur 3j, 7j, 15j (sans devoir recharger la page) representée graphiquement.
- Chaque utilisateur enregistré pourra rendre son profil d'humeur public.
- Chaque utilisateur enregistré pourra consulter l'humeur du jour des autres utilisateurs dont le profil est public.
- Un dashboard admin permet de lister,creer,modifier, supprimer les utilisateurs

1ere etape:
Determiner les taches a realiser, usercase, conception, choix des technos, etc

2eme etape:
Partage des taches sur trello

3eme etape:
Vous pouvez ouvrir votre IDE ...


# Modalités pédagogiques:
Travail en groupe de 4
Bonne repartition des taches

# Critères de performance:
Le site correspond au wireframe
Le site respecte la demande client initiale
Le site est adaptatif (responsive desktop / mobile)
Le site est en ligne
Les commits sont réguliers et explicites

# Modalités d'évaluation:
Presentation devant le groupe

# Livrables:
- Documents de conception (Wireframes, schema de base de données, usercase etc)
- Dépôt gitlab (obligatoire)
- Site en ligne (obligatoire)
