-- Creator:       MySQL Workbench 6.3.8/ExportSQLite Plugin 0.1.0
-- Author:        Nathan DUBURCQ
-- Caption:       New Model
-- Project:       Name of the project
-- Changed:       2022-02-15 16:25
-- Created:       2022-02-15 16:25
PRAGMA foreign_keys = OFF;

-- Schema: happy_db
BEGIN;
CREATE TABLE "roles"(
  "id" INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,
  "roleName" VARCHAR(45) NOT NULL
);
CREATE TABLE "users"(
  "id" INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,
  "created_at" TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
  "role_id" INTEGER NOT NULL,
  "username" VARCHAR(45) NOT NULL,
  "password" VARCHAR(65) NOT NULL,
  "email" VARCHAR(90) NOT NULL,
  "visibility" VARCHAR(45) NOT NULL,
  "modified_at" TIMESTAMP,
  "deleted_at" TIMESTAMP,
  CONSTRAINT "fk_user_role1"
    FOREIGN KEY("role_id")
    REFERENCES "roles"("id")
);
CREATE INDEX "users.fk_user_role1_idx" ON "users" ("role_id");
CREATE TABLE "humeurs"(
  "id" INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,
  "user_id" INTEGER,
  "humeur" VARCHAR(45) NOT NULL,
  "created_at" TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
  "modified_at" TIMESTAMP,
  "deleted_at" TIMESTAMP,
  CONSTRAINT "fk_humeur_user"
    FOREIGN KEY("user_id")
    REFERENCES "users"("id")
);
CREATE INDEX "humeurs.fk_humeur_user_idx" ON "humeurs" ("user_id");
COMMIT;
