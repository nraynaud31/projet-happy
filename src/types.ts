export type TUser = {
    id: number;
    username: string,
    email: string;
    password: string;
    created_at: string;
    modified_at: string;
    deleted_at: string;
}
