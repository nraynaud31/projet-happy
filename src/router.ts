import { Application } from "express";
import AdminDashboardController from "./controllers/AdminDashboardController";
import HomeController from "./controllers/HomeController";
import UserController from "./controllers/UserController";

export default function route(app: Application)
{
    /** Static pages **/
    app.get('/', (req, res) =>
    {
        HomeController.index(req, res);
    });

    /**
     * monsite.fr/user-all/
     */

    app.get('/profil', (req, res) =>
    {
        UserController.profil(req, res)
    });

    app.get('/user-all', (req, res) => 
    {
        UserController.listUsers(req, res)
    });

    app.get('/user-create', (req, res) =>
    {
        UserController.showForm(req, res)
    });

    app.post('/user-create', (req, res) =>
    {
        UserController.create(req, res)
    });

    app.get('/user-read/:id', (req, res) =>
    {
        UserController.read(req, res)
    });

    app.get('/users-update/:id', (req, res) =>
    {
        UserController.showFormUpdate(req, res)
    });

    app.post('/users-update/:id', (req, res) =>
    {
        UserController.update(req, res)
    });

    app.get('/user-delete/:id', (req, res) =>
    {
        UserController.delete(req, res)
    });

    app.get('/admin', (req, res) => {
        AdminDashboardController.index(req, res)
    })

    /*
    Pages de connexions
    */

    app.post('/login', (req, res) => {
        UserController.login(req, res);
    })

    app.get('/register', (req, res) => {
        HomeController.showRegister(req, res);
    })
    
    app.get('/login', (req, res) => {
        HomeController.showLogin(req, res);
    })

    app.post('/register', (req, res) => {
        UserController.register(req, res);
    })

    app.get('/logout', (req, res) => {
        UserController.logout(req, res);
    });


}
