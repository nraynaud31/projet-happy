import e from "express";
import { Request, Response } from "express-serve-static-core";
import User from "../models/User";

export default class AdminDashboardController {

    /**
     * Liste des utilisateurs
     * @param req Request
     * @param res Response
     */
    static index(req: Request, res: Response) {
        const db = req.app.locals.db;

        const users = User.getAll()

        // @ts-ignore
        if (req.session.role === 2) {
            res.render('pages/admin', {
                title: 'Panel admin',
                users: users,
            })
        } else {
            res.redirect('/')
        }



    }
}