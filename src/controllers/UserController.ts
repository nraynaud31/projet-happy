import { Request, Response } from "express-serve-static-core";
import bcrypt from 'bcrypt';
import User from "../models/User";

export default class UserController {
    /**
     * Affiche la liste des users
     * @param req 
     * @param res 
     */
    static index(req: Request, res: Response) {
        const db = req.app.locals.db;

        const users = User.getAll()

        res.render('pages/index', {
            title: 'users',
            users: users
        });
    }

        /**
    * Affiche le profil de l'user connecté
    * @param req 
    * @param res 
    */
         static profil(req: Request, res: Response): void {
            const db = req.app.locals.db;
    
            const users = User.getAllFromID(req.params.id)
    
            res.render('pages/profil', {
                users: users
            });
        }


    /**
     * Liste des utilisateurs qui possèdent la visibilité en public
     * @param req Request
     * @param res Response
     */
    static listUsers(req: Request, res: Response) {
        const db = req.app.locals.db;

        const users = User.getAllUsersWithPublicVisibility();

        res.render('pages/listusers', {
            title: 'users',
            users: users
        });
    }

    /**
     * Recupere le formulaire et insere l'users en db
     * @param req 
     * @param res 
     */
    static create(req: Request, res: Response): void {
        const db = req.app.locals.db;

        let hashed_password = bcrypt.hashSync(req.body.password, 1)
        User.createUser(req.body.title, req.body.content, hashed_password, "public", 1)
        UserController.index(req, res);
    }

    /**
    * Affiche 1 user
    * @param req 
    * @param res 
    */
    static read(req: Request, res: Response): void {
        const db = req.app.locals.db;

        const users = User.getAllFromID(req.params.id)

        res.render('pages/article', {
            users: users
        });
    }

    /**
    * Affiche le formulaire pour modifier un user
    * @param req 
    * @param res 
    */
    static showFormUpdate(req: Request, res: Response) {
        const db = req.app.locals.db;

        const users = User.getAllFromID(req.params.id)

        res.render('pages/users-update', {
            users: users
        });
    }


    /**
    * Recupere le formulaire de l'user modifié et l'ajoute a la database
    * @param req 
    * @param res 
    */
    static update(req: Request, res: Response) {
        const db = req.app.locals.db;

        let hashed_password = bcrypt.hashSync(req.body.password, 1)
        User.updateUser(req.body.title, req.body.content, hashed_password, req.params.id)

        UserController.index(req, res);
    }

    /**
     * Affiche le formulaire de creation d'users
     * @param req 
     * @param res 
     */
    static showForm(req: Request, res: Response): void {
        res.render('pages/users-create');
    }


    /**
     * Supprime un article
     * @param req 
     * @param res 
     */
    static delete(req: Request, res: Response) {
        const db = req.app.locals.db;

        User.deleteFromID(req.params.id)

        UserController.index(req, res);
    }

    /**
     * Système de login
     * @param req Request
     * @param res Response
     * @returns returns
     */
    static login(req: Request, res: Response): void {
        const db = req.app.locals.db;
        let password = req.body.password
        let email = req.body.email;
        const user = User.getByEmail(email);
        if (user === undefined) {
            res.redirect('/login')
            return
        }
        let comparaison_password = bcrypt.compareSync(password, user.password);
        if (user.email === email && comparaison_password) {
            //@ts-ignore
            req.session.email = email
            // @ts-ignore
            req.session.user = user.id;
            // @ts-ignore
            req.session.role = user.role_id;
            //@ts-ignore
            req.session.username = user.username;
            // @ts-ignore
            res.redirect('/')
        } else {
            console.log("L'ID n'est pas bon!");
            res.render('pages/login', {
                message: 'Identifiants incorrectes',
                title: 'Se connecter',
            })
        }
    }
    /**
     * Système de register
     * @param req Request
     * @param res Response
     */
    static register(req: Request, res: Response): void {
        const db = req.app.locals.db;
        let username = req.body.username;
        let email = req.body.email;
        let password = req.body.password;
        let hash = bcrypt.hashSync(password, 1);
        const user_register = User.getAll();
        let tableau: any[] = []
        // Permettre de vérifier si l'utilisateur est déjà inscrit avec cet adresse email
        const verif = User.getByEmail(email);
        user_register.forEach((element: any) => {
            tableau.push(element.email)
        });
        if (!tableau.includes(email)) {
            User.createUser(username, email, hash, "public", 1);
            res.redirect('/login')
        } else {
            res.render('pages/register', {
                title: "S'enregistrer",
                error: 'Erreur: un utilisateur possédant cet adresse email existe déjà dans la base de données !',
                verif: verif,
            })
        }
    }

    /**
     * Système de logout
     * @param req Request
     * @param res Response
     */
    static logout(req: Request, res: Response): void {
        if (req.session) {
            req.session.destroy(err => {
                if (!err) {
                    res.redirect('/')
                } else {

                }
            })
        } else {
            res.end();
        }
    }
}