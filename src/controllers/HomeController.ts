import { Request, Response } from "express-serve-static-core";

export default class HomeController
{
    static index(req: Request, res: Response): void
    {
        res.render('pages/index', {
            title: 'Welcome',
        });
    }

    /**
     * Affiche la page de register
     * 
     * @param req 
     * @param res 
     */
    static showRegister(req: Request, res: Response): void {
        res.render('pages/register', {
            title: "S'enregistrer",
        });
    }
/**
 * Affiche la page de Login
 * @param req Requête
 * @param res Réponse
 */
    static showLogin(req: Request, res: Response): void {
        res.render('pages/login', {
            title: 'Se connecter',
        });
    }

}