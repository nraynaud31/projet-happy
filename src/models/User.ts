import { RunResult } from 'better-sqlite3';
import {database} from '../server';
import { TUser } from '../types';

export default class User {

    static createUser(username: string, email: string, hashed_password: string, visibility: string, role_id: number): RunResult {
        return database.prepare('INSERT INTO users ("username", "email", "password", "visibility", "role_id") VALUES (?, ?, ?, ?, ?)').run(username, email, hashed_password, visibility, role_id);
    }

    static updateUser(id: string, username: string, email: string, password: string): RunResult{
        return database.prepare('UPDATE users SET username = ?, email = ?, password = ? WHERE id = ?').run(username, email, password, id);
    }

    static getByEmail(email: string): TUser {
        return database.prepare("SELECT * from users WHERE email=?").get(email);
    }

    static getAllUsersWithPublicVisibility(): any{
        return database.prepare('SELECT * FROM users WHERE visibility = ?').all('public')
        
    }
    
    static getAllFromID(id: string):TUser {
        return database.prepare('SELECT * FROM users WHERE id = ?').get(id);

    }

    static getAll(): any {
        return database.prepare("SELECT * FROM users").all();
    }

    static deleteFromID(id: string): RunResult {
        return database.prepare('DELETE FROM users WHERE id = ?').run(id);
    }
}